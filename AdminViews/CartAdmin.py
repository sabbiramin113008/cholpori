# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 19 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""
from flask import flash
from flask_admin.contrib.peewee import ModelView
from flask_admin.form import SecureForm
from wtforms.validators import AnyOf

from AdminViews import *
from Models.Cart import Cart


class CartAdmin(ModelView):
    form_base_class = SecureForm

    can_view_details = True
    create_modal = True
    can_create = True
    page_size = 50
    column_type_formatters = MY_DEFAULT_FORMATTERS
    column_labels = dict(
        c_data='Created',
        u_data='Updated',
    )

    column_default_sort = ('c_data', True)

    form_excluded_columns = ('u_data')

    column_exclude_list = ['u_data']
    column_searchable_list = []
    form_args = {
        'status': {
            'label': 'Active Status',
            'validators': [AnyOf(['Active', 'Inactive'])],
            'choices': [
                ('Active', 'Active'),
                ('Inactive', 'InActive')
            ]
        }
    }
