# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 19 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

from datetime import date, datetime

from flask_admin.model import typefmt


def format_datetime(view, value):
    return value.strftime('%a %I:%M %p, %b %d, %Y')


def format_date(view, value):
    return value.strftime('%b %d, %Y')


MY_DEFAULT_FORMATTERS = dict(typefmt.BASE_FORMATTERS)
MY_DEFAULT_FORMATTERS.update({
    type(None): typefmt.null_formatter,
    datetime: format_datetime,
    date: format_date
})
