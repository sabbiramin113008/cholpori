# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 19 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""
from authlib.integrations.flask_client import OAuth
from flask import Flask, render_template, request
from flask import redirect
from flask import url_for, session
from flask_admin import Admin

from AdminViews.CartAdmin import CartAdmin
from AdminViews.CartProductAdmin import CartProductAdmin
from AdminViews.CategoryAdmin import CategoryAdmin
from AdminViews.ProductAdmin import ProductAdmin
from AdminViews.UserAdmin import UserAdmin
from Facility.ProductFacility import ProductFacility
from Models import User, Category, Product, Cart, CartProducts, Order
from SearchUtils.Search import SearchUtil

app = Flask(__name__)
app.secret_key = '!secret'
app.config.from_object('config')

CONF_URL = 'https://accounts.google.com/.well-known/openid-configuration'
oauth = OAuth(app)
oauth.register(
    name='google',
    server_metadata_url=CONF_URL,
    client_kwargs={
        'scope': 'openid email profile'
    }
)

coupons = {
    'cholpori20': {
        'percentage': 10,
        'maxp': 200
    },
    'cholporiMax100': {
        'percentage': 5,
        'maxp': 150
    }
}


def get_u_name():
    try:
        mod = session.get('user')
        u_name = dict()
        u_name['name'] = mod.get('name')
        u_name['mail'] = mod.get('email')
        u_name['image'] = mod.get('picture')
    except:
        u_name = None
    return u_name


@app.route('/')
@app.route('/jeans')
def jeans():
    product_facility = ProductFacility()
    response = product_facility.get_product_by_category(cat='jeans'.upper(), page=1, size=16)
    return render_template('category.html',
                           u_name=get_u_name(),
                           cat_name='jeans'.upper(),
                           cat_name_small='jeans',
                           blocks=response.get('blocks'),
                           items_count=response.get('items_count'))


@app.route('/foods')
def foods():
    product_facility = ProductFacility()
    response = product_facility.get_product_by_category(cat='foods'.upper(), page=1, size=16)
    return render_template('category.html',
                           u_name=get_u_name(),
                           cat_name='foods'.upper(),
                           cat_name_small='foods',
                           blocks=response.get('blocks'),
                           items_count=response.get('items_count'))


@app.route('/search')
def search():
    return render_template('search.html', u_name=get_u_name())


@app.route('/result')
def get_search_result():
    token = request.args["term"]
    s_util = SearchUtil()
    response = s_util.search_for_autocomplete(token)
    length = len(response)
    return render_template('result.html', u_name=get_u_name(), token=token, response=response, rl=str(length))


@app.route('/cat/<cat>')
def get_category_page(cat):
    product_facility = ProductFacility()
    response = product_facility.get_product_by_category(cat=str(cat).upper(), page=1, size=8)
    return render_template('category.html', cat_name=str(cat).upper(),
                           blocks=response.get('blocks'),
                           items_count=response.get('items_count'))


@app.route('/i')
def i():
    return render_template('home.html')


# Cart and Stuffs
@app.route('/cart')
def add_to_cart():
    u_name = get_u_name()
    if not u_name:
        return redirect('/login')
    mail = u_name.get('mail')
    try:
        product_id = request.args['product_id']
        pro_repo = Product.Product()
        isProduct, product = pro_repo.get_product_by_id(product_id)
        if not isProduct:
            return redirect('/')

        cart_repo = Cart.Cart()
        sts, mCart = cart_repo.insert_cart(mail=mail)
        if not sts:
            return redirect('/login')
        cart_product_repo = CartProducts.CartProducts()
        hasCart, response = cart_product_repo.insert_product(cart=mCart, product=product)
        if hasCart:
            total_price = response.get('total_price')
            items = response.get('items')
            session['cart_id'] = mCart.id
            return render_template('cart.html',
                                   cart_id=mCart.id,
                                   u_name=u_name,
                                   total_price='৳ {}'.format(total_price),
                                   items=items
                                   )
    except:

        cart_repo = Cart.Cart()
        sts, carts = cart_repo.get_cart_by_mail(mail)
        if not sts:
            return redirect('/')

        cart_product_repo = CartProducts.CartProducts()
        response = cart_product_repo.get_all_products_by_cart(cart_id=carts[0].id)
        total_price = response.get('total_price')
        items = response.get('items')
        session['cart_id'] = str(carts[0].id)
        return render_template('cart.html',
                               cart_id=str(carts[0].id),
                               u_name=u_name,
                               total_price='৳ {}'.format(total_price),
                               items=items
                               )


@app.route('/remove')
def remove_product():
    cart_id = request.args['cart_id']
    pid = request.args['pid']

    u_name = get_u_name()

    cart_product_repo = CartProducts.CartProducts()
    sts, response = cart_product_repo.remove_product(cart_id=cart_id, product_id=pid)
    if sts:
        total_price = response.get('total_price')
        items = response.get('items')
        session['cart_id'] = cart_id
        return render_template('cart.html',
                               cart_id=cart_id,
                               u_name=u_name,
                               total_price='৳ {}'.format(total_price),
                               items=items
                               )


# Place Orders
@app.route('/placeorder')
def placeorder():
    u_name = get_u_name()
    if not u_name:
        return redirect('/login')
    try:
        cart_id = session.get('cart_id')
        try:
            coupon = request.args['coupon']
        except:
            coupon = None
        response = CartProducts.CartProducts.get_all_products_by_cart(cart_id=cart_id)
        total_price = response.get('total_price')
        coup = coupons.get(coupon, None)
        if not coupon or not coup:
            payable_price = total_price
            sts, order = Order.Order.insert_order(cart_id=cart_id,
                                                  total_price=total_price,
                                                  payable_price=payable_price)
            if sts:
                session['cart_id'] = None
                return render_template('placeorder.html',
                                       u_name=u_name,
                                       total_price=str(total_price),
                                       payable_price=str(payable_price),
                                       order_id=str(order.id)
                                       )
            else:
                return redirect('/')

        if coup:
            percentage = int(coup.get('percentage'))
            maxp = int(coup.get('maxp'))
            delta = round(total_price * percentage / 100, 3)
            import operator
            discount = maxp if operator.le(maxp, delta) else delta
            payable_price = round(total_price - discount, 3)
            sts, order = Order.Order.insert_order(cart_id=cart_id,
                                                  total_price=total_price,
                                                  payable_price=payable_price)
            if sts:
                session['cart_id'] = None
                return render_template('placeorder.html',
                                       u_name=u_name,
                                       total_price=str(total_price),
                                       payable_price=str(payable_price),
                                       order_id=str(order.id)
                                       )

            else:
                return redirect('/')
    except:
        return redirect('/')


# Login and Stuffs
@app.route('/login')
def login():
    redirect_uri = url_for('auth', _external=True)
    return oauth.google.authorize_redirect(redirect_uri)


@app.route('/google/callback')
def auth():
    token = oauth.google.authorize_access_token()
    user = oauth.google.parse_id_token(token)
    if user.get('email_verified'):
        session['user'] = user
        name = user.get('name')
        mail = user.get('email')
        user_repo = User.User()
        sts = user_repo.insert_user(mail=mail, name=name)
        return redirect('/')


@app.route('/logout')
def logout():
    session.pop('user', None)
    return redirect('/')


if __name__ == '__main__':
    admin = Admin(app, name='Cholpori Admin')
    admin.add_view(UserAdmin(User.User, category='Auth'))
    admin.add_view(CategoryAdmin(Category.Category, category='Category'))
    admin.add_view(ProductAdmin(Product.Product, category='Product'))
    admin.add_view(CartAdmin(Cart.Cart, category='Cart'))
    admin.add_view(CartProductAdmin(CartProducts.CartProducts, category='Cart Products'))

    app.run(
        host='127.0.0.1',
        port=7758,
        debug=True
    )
