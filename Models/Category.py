# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 19 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""


from Models.PrimeModels import *

class Category(BaseModel):
    name = CharField(null=False, unique=True)

    def __str__(self):
        return self.name