# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 19 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""
from Models import User, Cart, Category, Product, CartProducts, Order
from Models.PrimeModels import *


def init_db_lite():
    app_db.connect()
    user = User.User()
    cat = Category.Category()
    product = Product.Product()
    cart = Cart.Cart()
    cp = CartProducts.CartProducts()
    order = Order.Order()

    tables = [user, cat, product, cart, cp, order]
    print(app_db.create_tables(tables))
    app_db.close()


if __name__ == '__main__':
    init_db_lite()
