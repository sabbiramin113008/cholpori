# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 19 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

from Models.Cart import Cart
from Models.PrimeModels import *


class Order(BaseModel):
    cart = ForeignKeyField(Cart, backref='carts')
    total_price = DoubleField(null=True)
    payable_price = DoubleField(null=True)

    @classmethod
    def insert_order(cls, cart_id, total_price, payable_price):
        sts, cart = Cart.get_cart_by_id(cart_id=cart_id)
        if sts:
            try:
                order = Order(
                    cart=cart,
                    total_price=total_price,
                    payable_price=payable_price
                )
                order.save()
                cart.status = 'Inactive'
                cart.save()
                return 1, order
            except:
                return 0, ''
        return 0, ''
