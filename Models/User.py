# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 19 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

from Models.PrimeModels import *


class User(BaseModel):
    name = CharField(null=True)
    mail = CharField(null=False, unique=True, index=True)
    mobile = CharField(null=True)
    password = CharField(null=True)

    def __str__(self):
        return self.mail

    @classmethod
    def get_user_by_mail(cls, mail):
        sts = 0
        with app_db.atomic():
            try:
                user = cls.get(User.mail == mail)
                sts = 1
            except DoesNotExist:
                user = ''
        app_db.close()
        return sts, user

    @classmethod
    def insert_user(cls, mail, name):
        with app_db.atomic():
            try:
                user = User(
                    mail=mail,
                    name=name
                )
                user.save()
                sts = 1
            except IntegrityError:
                sts = 0
        return sts
