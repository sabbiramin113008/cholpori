# -*- coding: utf-8 -*-
"""
author: S.M. Sabbir Amin
data: 8/14/2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

db_config = {
    'db_name': 'cholpori_one',
    'user': '<use_db_name>',
    'password': '<use_db_password>',
    'host': 'localhost',
    'port': 3306
}
