# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 19 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

from Models.Category import Category
from Models.PrimeModels import *


class Product(BaseModel):
    title = CharField(null=False, unique=True, index=True)
    description = TextField(null=True)
    price = DoubleField(default=0.0)
    image = CharField(null=True)
    category = ForeignKeyField(Category, backref='products')

    def __str__(self):
        return self.title

    @classmethod
    def get_product_by_category(cls, cat, page, size):
        products = list()
        try:
            total_count = Product.select().join(Category).where(Category.name == cat).count()
        except:
            total_count = 0
        for p in Product.select().join(Category) \
                .where(Category.name == cat) \
                .paginate(page, size):
            products.append(p)
        return total_count, products

    @classmethod
    def get_all_product_for_autocomplete_index(cls):
        ps = []
        for p in Product.select():
            ps.append(p)
        return ps

    @classmethod
    def get_product_by_id(cls, pid):
        try:
            pro = Product.get(Product.id == pid)
            return 1, pro
        except DoesNotExist:
            return 0, ""
