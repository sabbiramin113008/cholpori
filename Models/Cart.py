# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 19 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

from Models.PrimeModels import *
from Models.User import User


class Cart(BaseModel):
    user = ForeignKeyField(User, backref='users')
    status = CharField(default='Active')

    def __str__(self):
        return self.user.mail

    @classmethod
    def insert_cart(cls, mail):
        sts, user = User.get_user_by_mail(mail)
        if sts:
            try:
                q = Cart.get(Cart.user == user, Cart.status == 'Active')
                return 1, q
            except DoesNotExist:
                try:
                    q = Cart(
                        user=user
                    )
                    q.save()
                    return 1, q
                except Exception as e:
                    return 0, ''
        return 0, ''

    @classmethod
    def get_cart_by_mail(cls, mail):
        carts = list()
        for c in Cart.select().join(User).where(User.mail == mail, Cart.status == 'Active'):
            carts.append(c)
        return len(carts), carts

    @classmethod
    def get_cart_by_id(cls, cart_id):
        try:
            c = Cart.get(Cart.id == cart_id)
            return 1, c
        except:
            return 0, ''
