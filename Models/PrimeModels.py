# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 19 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""
import os
from datetime import datetime

from peewee import *
from playhouse.pool import PooledMySQLDatabase, PooledSqliteDatabase

from Models.Configs import db_config


def get_dbcontext():
    return PooledMySQLDatabase('{}'.format(db_config["db_name"]),
                               user=db_config["user"],
                               password=db_config["password"],
                               host=db_config["host"],
                               port=db_config["port"],
                               timeout=15,
                               max_connections=1000,
                               stale_timeout=15)


def lite_db():
    DB_Path = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        '{}.db'.format(db_config["db_name"])
    )
    return PooledSqliteDatabase(DB_Path,
                                timeout=10,
                                check_same_thread=False,
                                max_connections=500,
                                stale_timeout=20)

#using mysql db
# app_db = get_dbcontext()
#using sqlite db
app_db = lite_db()


class BaseModel(Model):
    c_data = DateTimeField(default=datetime.now)
    u_data = DateTimeField(null=True)

    class Meta:
        database = app_db

class Coupon(BaseModel):
    coupon_type = CharField(null=False, unique=True)
    coupon_value = IntegerField(default=0)
