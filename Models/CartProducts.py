# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 19 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

from Models.Cart import Cart
from Models.PrimeModels import *
from Models.Product import Product


class CartProducts(BaseModel):
    cart = ForeignKeyField(Cart, backref='carts')
    product = ForeignKeyField(Product, backref='products')

    @classmethod
    def get_all_products_by_cart(cls, cart_id):
        ps = list()
        total_price = 0
        for cp in CartProducts.select().join(Cart).where(Cart.id == cart_id):
            mod = {
                'pid': cp.product.id,
                'cart_id': cp.cart.id,
                'title': cp.product.title,
                'image': cp.product.image,
                'price': cp.product.price
            }
            total_price += cp.product.price
            ps.append(mod)
        response = dict()
        response['total_price'] = total_price
        response['items'] = ps
        return response

    @classmethod
    def insert_product(cls, cart, product):
        try:
            cproduct = CartProducts(
                cart=cart,
                product=product
            )
            cproduct.save()
            response = cls.get_all_products_by_cart(cart_id=cproduct.cart.id)
            return 1, response
        except:
            return 0, ''

    @classmethod
    def remove_product(cls, cart_id, product_id):
        sts, p = Product.get_product_by_id(pid=product_id)
        stts, c = Cart.get_cart_by_id(cart_id)
        if stts and sts:
            q = CartProducts.get(CartProducts.cart == c, CartProducts.product == p)
            q.delete_instance()

            response = cls.get_all_products_by_cart(cart_id)
            return 1, response
        return 0, ''
