# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 20 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

from walrus import *

from Models.Product import Product


class SearchUtil:
    def __init__(self):
        self.db = Database(host='localhost', port=6379, db=1)
        self.autocomplete = self.db.autocomplete()
        self.product_repo = Product()

    def prepare_index_for_auto_complete(self):
        products = self.product_repo.get_all_product_for_autocomplete_index()
        for product in products:
            meta = {
                'product_id': product.id,
                'title': product.title,
                'image': product.image,
                'price': '৳{}'.format(product.price)
            }
            self.autocomplete.store(
                obj_id=product.id,
                title=product.title,
                data=meta,
                obj_type='entry'
            )
        print('Autocomplete Indexing is Done')

    def search_for_autocomplete(self, token):
        responses =  self.autocomplete.search(phrase=token)
        rr = list()
        for r in responses:
            rr.append(r)
        return rr

    def do_exits(self, token):
        return self.autocomplete.exists(token)

# # #
# sutil = SearchUtil()
# sutil.prepare_index_for_auto_complete()
# responses = sutil.search_for_autocomplete(token='deni')
# for r in responses:
#     print (r)
# #
