# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 20 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""
from SearchUtils.Search import SearchUtil


def prepare_auto_indexing():
    sutil = SearchUtil()
    sutil.prepare_index_for_auto_complete()


if __name__ == '__main__':
    prepare_auto_indexing()
