# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 20 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

Auth_Redirect_URL = 'http://127.0.0.1:7758/google/callback'
Base_URL = 'http://127.0.0.1:7758'
GOOGLE_CLIENT_ID = '<Your-Google-Client-Id'
GOOGLE_CLIENT_SECRET = '<Your-Google-Client-Secret>'
