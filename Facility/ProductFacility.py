# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 19 Aug 2020
email: sabbir@rokomari.com, sabbiramin.cse11ruet@gmail.com

"""

from Models.Product import Product


class ProductFacility:
    def __init__(self):
        self.product_repo = Product()

    def get_product_by_category(self, cat, page, size):
        total, items = self.product_repo.get_product_by_category(cat, page, size)
        mega = list()
        response = dict()
        block = list()
        response['total'] = total
        response['items_count'] = len(items)
        for indx, item in enumerate(items):

            if len(block) == 2:
                block_copy = block.copy()
                mega.append(block_copy)
                block.clear()
                mod = dict()
                mod['id'] = item.id
                mod['title'] = item.title
                mod['details'] = item.description
                mod['price'] = str(item.price)
                mod['image'] = item.image
                block.append(mod)
            else:
                mod = dict()
                mod['id'] = item.id
                mod['title'] = item.title
                mod['details'] = item.description
                mod['price'] = str(item.price)
                mod['image'] = item.image
                block.append(mod)
        mega.append(block)
        response['blocks'] = mega
        return response
