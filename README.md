# Cholpori 
A Demo e-commerce with several utilities.

## Technologies Used:
1. Language- `Python  3.6.5` 
2. Micro Web Framework - `Flask`
3. Database Storage - `SQLite` and `MySQL` both.
4. In Memory Storage - `Redis`
5. Social Login Used- `Google Login`.
6. WSL for Virtual Unix Tools.
   
## Application Features
1. Pattern Followed - `MVC`
2. Builder Patterns for Object Initiation.

## Features
1. Admin Panel for Data Entry and Manipulation.
2. Mobile, Desktop Responsiveness.
3. Mostly Server Side Rendered.
4. Auto Complete Search Indexing using Redis.

# How To Run the application

## Pre Requisites. 
0. Download and install Python. 
   a. Global installing of packages used in this project is discouraged.
   b. Use Virtual Env for this project.
1. Install the dependencies from `requirements.txt`.
   1. `pip install -r requirements.txt` should work, if not, then 
   2. `pip install <package_name>` for all the pacakges.
2. Activate the virtual env. 
   
## Database Preparation.
1. I've added a `SQLite` version of the database within this project as `cholpori_one.db`. 
2. Further db settings can be found under `./cholpori/Models/Configs.py` file.
3. For Schema Generation, use `./cholpori/Models/init_db.py`. No need to execute this, if SQLite database is used.
4. Use `./cholpori/Models/Config.py` settings for `MySQL` credentials.

## Preparation of Redis.
NB. Installing of Redis is beyond the scope. I've used WSL where Ubuntu is used as a host machine for Redis. 
1. Redis is used for storing search indice in this project.
2. If `locahost`,`port` and `db` are to be changed, then please follow `./cholpori/SearchUtils/Search.py` file.
3. Please run `./cholpori/SearchUtils/init_indexing.py` to store the indice in Redis for the first time.

## Google Credentials for oAuth2 Client
NB. Due to security measure, I am not sharing `client_id` or `client_secret` within this project.
1. Please Create your Google OAuth Client at https://console.cloud.google.com/apis/credentials, make sure to add `http://127.0.0.1:7758/google/callback` into Authorized redirect URIs.
2. For further changes, please follow along`./cholpori/config.py` file.
   
## Running the App
1. Open any terminal and run `python server.py` from the root dir of this project.
Voila. 

## Endpoints:
1. Base URL: `http://127.0.0.1:7758`
2. Admin URL: `http://127.0.0.1:7758/admin`

## Comments:
1. Total 27 Hours have been spent for this project.
2. Many Production level implementations have been skipped due to very tight schedule. 
3. Several screenshots are added in `screenshots` dir.
4. Sample Data are Added in `resources` dir.